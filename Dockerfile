#FROM ubuntu:18:04
FROM python:3.7-alpine3.8

# Install python3 and python3-pip
#run apt update
#run apt install -y python3
#run apt install -y python3-pip

# Copy application files into the container
copy . /testcapstone

# Install required python libraries
run python3 -m pip install -r /testcapstone/testcapstone_requirements.txt

# Have the container expose port 8080
#EXPOSE 8080

CMD python3 /testcapstone/solar.py



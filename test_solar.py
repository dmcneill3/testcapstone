#!/usr/bin/env python3
from unittest.mock import patch
from solar import get_observer_location

def test_get_observer_location_success():
    with patch('requests.get') as mock_get:
        mock_response = mock_get.return_value
        mock_response.status_code = 200
        mock_response.json.return_value = {
            'lat': '42.898723',
            'lon': '-70.81294'
        }

        lat1, lon1 = get_observer_location()
        mock_get.assert_called_once()
        assert lat1 == '12.898723'
        assert lon1 == '-70.81294'
        print("successful")

def test_get_observer_location_error():
    with patch('requests.get') as mock_get:
        mock_response = mock_get.return_value
        mock_response.status_code = 500
        mock_response.json.return_value = {
            'lat': '42.898723',
            'lon': '-70.81294'
        }

        lat1, lon1 = get_observer_location()
        mock_get.assert_called_once()
        assert lat1 == ''
        assert lon1 == ''
        print('error')

print("Hello")

